<?php

class Person {
    
    private $email;
    private $name;
    private $date;
    private $site;
    private $personalData;


    public function __construct($name, $email, $data, $site) {
        $this->email = $email;
        $this->name = $name;
        $this->date = $data;
        $this->site = $site;
    }
    
    public function getMail(){ 
        return "<a href=mailto:$this->email>Отправить e-mail $this->name</a>";
    }
    
    public function getName(){
        return $this->name;
    }
            
    public function getDate(){
        return date_format(date_create($this->date), "F d, Y");
    }
    
    public function getSite(){
        $this->site = $this->RightAddress($this->site);
        return "<a href=" . $this->site . ">$this->site</a>";
    }
    
    public function &getPersonData(){
        $this->personalData['FIO'] = $this->name;
        $this->personalData['Mail'] = $this->email;
        $this->personalData['Date'] = $this->date;
        $this->personalData['Site'] = $this->site;
        return $this->personalData;
    }

    private function RightAddress($str){
        if(substr($str, 0, 7) == 'http://'){
            $str = str_replace("http://", "", $str);
        }
        if(substr($str, 0, 4) == 'www.'){
            $str = str_replace("www.", "", $str);
        }
            return 'http://www.' . $str;
    }
}

?>