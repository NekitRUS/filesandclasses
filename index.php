<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require_once 'GetData.php';
        require_once 'Person.php';
        
        $usersInfo = array();
        $data = new GetData();
        foreach ($data->GetFileLIst() as $file){
            $info = $data->GetTextFileContent($file);
            $usersInfo[] = new Person($info[0], $info[1], $info[2], $info[3]);
        }
        
        echo "<center><H1>Список пользователей:</H1>";
        echo "<table border='black'>
            <tr>
                <th>ФИО</th>
                <th>E-mail</th>
                <th>Дата регистрации</th>
                <th>Web-адрес</th>
            </tr>";
        foreach ($usersInfo as $info){
            echo "<tr><td>" . $info->getName() . "</td>";
            echo "<td>" . $info->getMail() . "</td>";
            echo "<td>" . $info->getDate() . "</td>";
            echo "<td>" . $info->getSite() . "</td></tr>";
        }

        ?>
    </body>
</html>
