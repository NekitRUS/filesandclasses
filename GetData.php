<?php

class GetData {
    
    Public static $dir = __DIR__;
    
    Public function SetDir($d){
        self::$dir = $d;
    }
    
    Public function GetFileLIst($filtr = "*.txt"){
        $files = glob(self::$dir . "\\" . $filtr);
        return array_map(function($file){return  substr($file, strrpos($file, "\\") + 1);}, $files);
    }
    
    Public function GetTextFileContent($file){
        return file($file);
    }
}

?>